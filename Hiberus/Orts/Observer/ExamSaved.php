<?php
namespace Hiberus\Orts\Observer;

use Hiberus\Orts\Model\Exam;
use Magento\Framework\EntityManager\EventManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class ExamSaved implements ObserverInterface
{
    /**
     * @var EventManager
     */
    private EventManager $eventManager;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * ExamSaved constructor.
     * @param EventManager $eventManager
     * @param LoggerInterface $logger
     */
    public function __construct(EventManager $eventManager, LoggerInterface $logger)
    {
        $this->eventManager = $eventManager;
        $this->logger = $logger;
    }

    /**
     * Logs entries to Magento's debug.log file
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            /** @var Exam */
            $exam = $observer->getEvent()->getDataObject();
            if ($exam && $exam->isObjectNew()) {
                $this->logger->debug("New object created: " . print_r($exam->toArray(), true));
            } else {
                $this->logger->debug("Object edited: " . print_r($exam->toArray(), true));
            }
        } catch (\Exception $e) {
            $this->logger->debug('Error message', ['exception' => $e]);
        }
    }
}
