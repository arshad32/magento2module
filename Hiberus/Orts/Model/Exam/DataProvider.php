<?php
namespace Hiberus\Orts\Model\Exam;

use Hiberus\Orts\Model\Exam;
use Hiberus\Orts\Model\ResourceModel\Exam\Collection;
use Hiberus\Orts\Model\ResourceModel\Exam\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * @author: Jose Manuel Orts
 * @date: 24/07/2020
 */

class DataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
    */
    protected $collection;
    /**
     * @var array
    */
    protected array $_loadedData;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $examCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $examCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }

        $items = $this->collection->getItems();

        /**@var Exam */
        foreach ($items as $exam) {
            $this->_loadedData[$exam->getData('id_exam')] = $exam->getData();
        }
        return $this->_loadedData;
    }
}
