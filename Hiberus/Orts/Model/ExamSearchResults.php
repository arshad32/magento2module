<?php
namespace Hiberus\Orts\Model;

use Hiberus\Orts\Api\Data\ExamSearchResultsInterface;
use Magento\Framework\Api\SearchResults;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

class ExamSearchResults extends SearchResults implements ExamSearchResultsInterface
{

}
