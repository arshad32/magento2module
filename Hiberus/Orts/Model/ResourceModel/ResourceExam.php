<?php
namespace Hiberus\Orts\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

class ResourceExam extends AbstractDb
{
    const TABLE = 'hiberus_exam';

    protected $_idFieldName = 'id_exam';

    protected function _construct()
    {
        $this->_init(self::TABLE, $this->_idFieldName);
    }
}
