<?php
namespace Hiberus\Orts\Controller\Index;

use Hiberus\Orts\Api\ExamRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

class Highest implements ActionInterface
{
    /**
     * @var JsonFactory
     */
    private JsonFactory $resultPageFactory;

    /**
     * @var ExamRepositoryInterface
     */
    private ExamRepositoryInterface $examRepository;

    /**
     * Index constructor.
     * @param Context $context
     * @param JsonFactory $resultPageFactory
     * @param ExamRepositoryInterface $examRepository
     */
    public function __construct(
        Context $context,
        JsonFactory $resultPageFactory,
        ExamRepositoryInterface $examRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->examRepository = $examRepository;
    }

    public function execute()
    {
        return $this->resultPageFactory->create()->setData(
            [
                'highest' => $this->examRepository->getHighestMark()
            ]
        );
    }
}
