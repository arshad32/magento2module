<?php
namespace Hiberus\Orts\Controller\Adminhtml\Menu;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * @author: Jose Manuel Orts
 * @date: 23/07/2020
 */

class Index implements ActionInterface
{
    /**
     * @var PageFactory
     */
    protected PageFactory $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
    }
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Hiberus_Orts::menu');
        $resultPage->getConfig()->getTitle()->prepend(__('Exams Management'));
        return $resultPage;
    }
}
