<?php
namespace Hiberus\Orts\Controller\Adminhtml\Menu;

use Hiberus\Orts\Api\ExamRepositoryInterface;
use Hiberus\Orts\Model\ResourceModel\Exam\CollectionFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Ui\Component\MassAction\Filter;

/**
 * @author: Jose Manuel Orts
 * @date: 23/07/2020
 */
class MassDelete implements ActionInterface
{
    /**
     * @var Context
     */
    protected Context $context;
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $collectionFactory;
    /**
     * @var ExamRepositoryInterface
     */
    protected ExamRepositoryInterface $examRepository;
    /**
     * @var Filter
     */
    protected Filter $filter;
    /**
     * @var ManagerInterface
     */
    protected ManagerInterface $messenger;
    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $redirectFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param ExamRepositoryInterface $examRepository
     * @param CollectionFactory $collectionFactory
     * @param ManagerInterface $messenger
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        ExamRepositoryInterface $examRepository,
        CollectionFactory $collectionFactory,
        ManagerInterface $messenger,
        RedirectFactory $redirectFactory
    ) {
        $this->filter = $filter;
        $this->examRepository = $examRepository;
        $this->collectionFactory = $collectionFactory;
        $this->messenger = $messenger;
        $this->redirectFactory = $redirectFactory;
        $this->context = $context;
    }

    public function execute()
    {
        if (!$this->context->getRequest()->isPost()) {
            throw new CouldNotDeleteException(__('Bad Request'));
        }

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $count = 0;
        foreach ($collection->getItems() as $exam) {
            $this->examRepository->delete($exam);
            ++$count;
        }

        if ($count) {
            $this->messenger->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $count)
            );
        }

        return $this->redirectFactory->create()->setPath('orts/menu/index');
    }
}
