<?php
namespace Hiberus\Orts\Controller\Adminhtml\Menu;

use Hiberus\Orts\Api\Data\ExamInterface;
use Hiberus\Orts\Api\ExamRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * @author: Jose Manuel Orts
 * @date: 23/07/2020
 */
class Delete implements ActionInterface
{
    const ADMIN_RESOURCE = 'Index';

    /**
     * @var Context
     */
    protected Context $context;
    /**
     * @var RedirectFactory
    */
    protected RedirectFactory $redirectFactory;
    /**
     * @var ExamRepositoryInterface
     */
    protected ExamRepositoryInterface $examRepository;
    /**
     * @var ManagerInterface
     */
    protected ManagerInterface $messenger;
    public function __construct(
        Context $context,
        RedirectFactory $redirectFactory,
        ExamRepositoryInterface $examRepository,
        ManagerInterface $messenger
    ) {
        $this->context = $context;
        $this->redirectFactory = $redirectFactory;
        $this->examRepository = $examRepository;
        $this->messenger = $messenger;
    }

    public function execute()
    {
        $exam = $this->getExam();

        if (!$exam->getExamId()) {
            $this->messenger->addSuccessMessage(
                __('Exam not found.')
            );
            return $this->redirectFactory->create()->setPath('*/*/', ['_current' => true]);
        }

        $this->examRepository->delete($exam);
        $this->messenger->addSuccessMessage(__('Was removed successfully.'));

        return $this->redirectFactory->create()->setPath('orts/menu/index');
    }

    /**
     * @return ExamInterface | AbstractModel
     */
    public function getExam()
    {
        return $this->examRepository->load(
            $this->context->getRequest()->getParam('id')
        );
    }
}
