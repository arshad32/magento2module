<?php
namespace Hiberus\Orts\Test\Integration\Model;

use Hiberus\Orts\Model\Exam;
use Hiberus\Orts\Model\ResourceModel\ResourceExam;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;

/**
 * @author: Jose Manuel Orts
 */

class ExamTest extends Testcase
{
    /**
     * @var string
     */
    protected $firstName;
    /**
     * @var string
     */
    protected $lastName;
    /**
     * @var int
     */
    protected $mark;
    /**
     *  SUT
     * @var Exam
     */
    protected $exam;

    /**
     * @var ResourceExam
     */
    protected $resource;


    public function setUp(): void
    {
        $this->exam = Bootstrap::getObjectManager()->create(
            Exam::class
        );
        $this->resource = Bootstrap::getObjectManager()->create(
            ResourceExam::class
        );
        $this->firstName = 'Nikola';
        $this->lastName = 'Tesla';
        $this->mark = 10;
    }

    public function testShouldBeCreated()
    {
        self::assertInstanceOf(Exam::class, $this->exam);
    }

    public function testShouldSetAndGetProperties()
    {
        $this->exam->setFirstName($this->firstName)
            ->setLastName($this->lastName)
            ->setMark($this->mark);

        /**@var Exam $exam */
        $this->resource->save($this->exam);

        self::assertNotNull($this->exam->getExamId());
        self::assertEquals($this->firstName, $this->exam->getFirstName());
        self::assertEquals($this->lastName, $this->exam->getLastName());
        self::assertEquals($this->mark, $this->exam->getMark());

        $this->resource->delete($this->exam);
    }
}
