<?php


namespace Hiberus\Orts\Test\Unit\Plugin\fixtures;


use Hiberus\Orts\Api\Data\ExamInterface;

class ExamObject implements ExamInterface
{
    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $firstname
     */
    protected $firstname;
    /**
     * @var string $lastname
     */
    protected $lastname;
    /**
     * @param float $mark
     */
    protected $mark;

    /**
     * @return int
     */
    public function getExamId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setExamId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return $this
     */
    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return $this
     */
    public function setLastName($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return float
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * @param float $mark
     * @return $this
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * @param RestExamPostParamContainer $examPostParam
     */
    public function setDataFromRequest(ExamInterface $examPostParam)
    {
        return $this;
    }

}
