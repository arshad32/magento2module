<?php
namespace Hiberus\Orts\Test\Unit\Plugin;

use Hiberus\Orts\Block\Index;
use Hiberus\Orts\Model\Exam;
use Hiberus\Orts\Plugin\Unapproved;
use Hiberus\Orts\Test\Unit\Plugin\fixtures\ExamObject;
use PHPUnit\Framework\TestCase;
use Magento\TestFramework\Helper\Bootstrap;

/**
 * @author: Jose Manuel Orts
 */

class UnapprovedTest extends TestCase
{
    /**
     * SUT
     * @var Unapproved $plugin
    */
    protected $plugin;


    public function setUp(): void
    {
        $this->plugin = Bootstrap::getObjectManager()->create(
            Unapproved::class
        );
    }

    public function testShouldBeCreated()
    {
        self::assertInstanceOf(Unapproved::class, $this->plugin);
    }

    public function testShouldDoNothing()
    {
        $mark = 10;
        $indexBlock = $this->getMockBuilder(Index::class)
            ->disableOriginalConstructor()
            ->getMock();
        $examOne = (new ExamObject())
            ->setMark(10);
        $data = [$examOne];

        $result = $this->plugin->afterGetExams($indexBlock, $data);

        self::assertEquals($mark, $result[0]->getMark());
    }

    public function testShouldSetPredeterminedMarkToFailedExam()
    {
        $markOne = 10;
        $markTwo = 4;
        $indexBlock = $this->getMockBuilder(Index::class)
            ->disableOriginalConstructor()
            ->getMock();
        $examOne = (new ExamObject())
            ->setMark(10);
        $examTwo = (new ExamObject())
            ->setMark(4);

        $data = [$examOne, $examTwo];

        $result = $this->plugin->afterGetExams($indexBlock, $data);

        self::assertEquals(Unapproved::UNAPPROVED_GENERAL_VALUE, $result[1]->getMark());
    }
}
