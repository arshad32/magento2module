<?php
/**
 * @author: Jose Manuel Orts
 * @date: 16/07/2020
 */
use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Hiberus_Orts', __DIR__);
