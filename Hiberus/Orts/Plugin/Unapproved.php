<?php
namespace Hiberus\Orts\Plugin;

use Hiberus\Orts\Api\Data\ExamInterface;
use Hiberus\Orts\Block\Index;

/**
 * @author: Jose Manuel Orts
 * @date: 22/07/2020
 */
class Unapproved
{
    const APPROVED_MARK = 5;
    const UNAPPROVED_GENERAL_VALUE = 4.99;

    /**
     * @param Index $subject
     * @param $result
     * @return mixed
     */
    public function afterGetExams(Index $subject, $result)
    {
        /**@var ExamInterface $item */
        foreach ($result as $item) {
            if ($item->getMark() < self::APPROVED_MARK) {
                $item->setMark(self::UNAPPROVED_GENERAL_VALUE);
            }
        }
        return $result;
    }
}
