<?php
namespace Hiberus\Orts\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * @author: Jose Manuel Orts
 * @date: 17/07/2020
 */

class AddData implements DataPatchInterface, PatchVersionInterface
{
    const FILE_DATA = __DIR__ . '/students.csv';

    /**
     * @var ModuleDataSetupInterface
     */
    protected ModuleDataSetupInterface $moduleDataSetup;

    /**
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(ModuleDataSetupInterface $moduleDataSetup)
    {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $this->moduleDataSetup->getConnection()
            ->insertMultiple(
                $this->moduleDataSetup->getTable('hiberus_exam'),
                $this->getFileData()
            );
        $this->moduleDataSetup->endSetup();
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
       return [];
    }

    public static function getVersion()
    {
        return '0.1.1';
    }

    private function getFileData()
    {
        $exponent = pow(10, 2);
        $fileData = [];
        $index = 0;

        if (($handle = fopen(self::FILE_DATA, 'r')) !== false) {
            $header = fgetcsv($handle, 500, ';');
            while (($row = fgetcsv($handle, 500, ';')) !== false) {
                $fileData[$index]['firstname'] = $row[0];
                $fileData[$index]['lastname'] = $row[1];
                $fileData[$index]['mark'] = mt_rand(0 * $exponent, 10 * $exponent) / $exponent;

                $log = new Logger('Logger');
                $log->pushHandler(new StreamHandler(__DIR__ . '/debug.log', Logger::DEBUG));
                $log->debug(__DIR__);
                $log->debug(implode(',', $fileData[$index]));
                ++$index;
            }
            fclose($handle);
        }
        return $fileData;
    }
}
