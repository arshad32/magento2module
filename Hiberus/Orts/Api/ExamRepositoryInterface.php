<?php
namespace Hiberus\Orts\Api;

use Hiberus\Orts\Api\Data\ExamInterface;
use Hiberus\Orts\Api\Data\ExamSearchResultsInterface;
use Magento\Framework\Api\SearchCriteria;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

interface ExamRepositoryInterface
{
    /**
     * @api
     *
     * @param \Hiberus\Orts\Api\Data\ExamInterface $exam
     * @return \Hiberus\Orts\Api\Data\ExamInterface
     */
    public function save(ExamInterface $exam): ExamInterface;

    /**
     * @api
     *
     * @param \Hiberus\Orts\Api\Data\ExamInterface $exam
     * @return bool
     */
    public function delete(ExamInterface $exam): bool;

    /**
     * @api
     *
     * Gets objects list
     *
     * @param \Magento\Framework\Api\SearchCriteria $criteria
     * @return \Hiberus\Orts\Api\Data\ExamSearchResultsInterface
     */
    public function getList(SearchCriteria $searchCriteria): ExamSearchResultsInterface;
}
