<?php
namespace Hiberus\Orts\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ExamSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Hiberus\Orts\Api\Data\ExamInterface[]
     */
    public function getItems();

    /**
     * @param \Hiberus\Orts\Api\Data\ExamInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
