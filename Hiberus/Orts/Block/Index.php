<?php
namespace Hiberus\Orts\Block;

use Hiberus\Orts\Api\ExamRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * @author: Jose Manuel Orts
 * @date: 21/07/2020
 */

class Index extends Template
{
    /**
     * @var ExamRepositoryInterface
    */
    private ExamRepositoryInterface $examRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    /**
     * @var SortOrderBuilder
     */
    private SortOrderBuilder $sortOrderBuilder;

    public function __construct(
        Context $context,
        ExamRepositoryInterface $examRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder

    ) {
        $this->examRepository = $examRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;

        parent::__construct($context);
    }

    public function getExams()
    {
        $_filter = $this->searchCriteriaBuilder->create();
        $sortOrder = $this->sortOrderBuilder->setField('firstname')
            ->setDirection(SortOrder::SORT_ASC)
            ->create();
        $_filter->setSortOrders([$sortOrder]);
        $list = $this->examRepository->getList($_filter);

        return $list->getItems();
    }
}
