<?php
namespace Hiberus\Orts\Block\Adminhtml\Menu;

use Magento\Cms\Block\Adminhtml\Block\Edit\GenericButton;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * @author: Jose Manuel Orts
 * @date: 23/07/2020
 */

class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    public function getButtonData()
    {
        return [
            'label' => __('Delete Exam'),
            'on_click' => 'deleteConfirm(\''
                . __('Are you sure you want to delete this exam ?')
                . '\', \''
                . $this->getDeleteUrl()
                . '\')',
            'class' => 'delete',
            'sort_order' => 20
        ];
    }

    public function getDeleteUrl()
    {
        return $this->getUrl(
            '*/*/delete',
            [
                'id' => $this->context->getRequest()->getParam('id')
            ]
        );
    }
}
