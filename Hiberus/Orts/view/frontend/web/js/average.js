define(['jquery', 'mage/url'], function($, url){
    "use strict";
    return function average()
    {
        $(function () {
            $.get(url.build('/orts/index/average') , function( data ) {
                $("#examList").append(
                    '<li class="average">Average: '+ data.average + '</li>'
                );
            }, "json");
        });
    }
});
