define(['jquery', 'Magento_Ui/js/modal/alert','mage/url'], function($, alert, url){
    "use strict";
    return function highest()
    {
        $('#highestMark').click(
            function () {
                $.get(url.build('/orts/index/highest') , function( data ) {
                    alert({
                        title: $.mage.__('The highest mark is'),
                        content: $.mage.__(data.highest),
                    });
                }, "json");
            }
        );
    }
});
