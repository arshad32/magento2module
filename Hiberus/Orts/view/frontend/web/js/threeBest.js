define(['jquery'], function($){
    "use strict";
    return function threeBest()
    {
        let marks = [];
        $('#examList li .mark').each(function() {
            marks.push(parseFloat($(this).text()));
        });
        marks.sort(function(a, b){return b-a});
        let featured = {
            "color": "green",
            "font-size": "16px",
            "font-weight": "bold",
            "background-color": "#b6f9b6",
        };
        $( `#examList li .mark:contains('${marks[1]}')` ).parent().css(featured);
        $( `#examList li .mark:contains('${marks[2]}')` ).parent().css(featured);
        $( `#examList li .mark:contains('${marks[3]}')` ).parent().css(featured);
    }
});
