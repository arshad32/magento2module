var config = {
    map: {
        '*': {
            exam: 'Hiberus_Orts/js/exam',
            highest: 'Hiberus_Orts/js/highest',
            average: 'Hiberus_Orts/js/average',
            threeBest: 'Hiberus_Orts/js/threeBest',
        }
    }
};
